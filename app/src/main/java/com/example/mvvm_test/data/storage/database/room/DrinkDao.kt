package com.example.mvvm_test.data.storage.database.room

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.mvvm_test.data.repository.model.Drink

@Dao
interface DrinkDao {
    @Query("SELECT * FROM drink")
    fun getAll(): /*LiveData<List<Drink>>*/List<Drink>

    @Query("SELECT * FROM drink WHERE name = :name")
    fun getById(name: String): Drink?

    @Insert
    fun insertDrink(drink: Drink)
}