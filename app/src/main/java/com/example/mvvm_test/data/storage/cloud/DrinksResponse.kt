package com.example.mvvm_test.data.storage.cloud

import com.example.mvvm_test.data.storage.model.DrinkDto
import com.google.gson.annotations.SerializedName

data class DrinksResponse(
    @SerializedName("drinks") var drinkDtoList: List<DrinkDto>
)

