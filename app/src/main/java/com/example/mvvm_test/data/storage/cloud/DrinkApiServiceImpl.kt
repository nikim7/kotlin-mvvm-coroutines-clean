package com.example.mvvm_test.data.storage.cloud

import com.example.mvvm_test.BuildConfig
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class DrinkApiServiceImpl {

     fun getRandomDrinkAsync(): Deferred<DrinksResponse> {
        return Retrofit.Builder().baseUrl(BuildConfig.baseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .build()
            .create(DrinkApiService::class.java)
            .getRandomDrinkAsync()
    }
}

