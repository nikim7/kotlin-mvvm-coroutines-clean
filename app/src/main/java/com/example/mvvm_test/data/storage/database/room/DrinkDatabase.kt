package com.example.mvvm_test.data.storage.database.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.mvvm_test.data.repository.model.Drink

@Database(entities = [Drink::class], version = 1)
abstract class DrinkDatabase: RoomDatabase() {
    abstract val drinkDao: DrinkDao

    companion object {
        private var INSTANCE: DrinkDatabase? = null

        fun createAppDatabase(context: Context): DrinkDatabase? {
            if (INSTANCE == null){
                synchronized(DrinkDatabase::class){
                    INSTANCE = Room.databaseBuilder(context, DrinkDatabase::class.java, "myDB").build()
                }
            }
            return INSTANCE
        }

        fun getReadyDatabase(): DrinkDatabase?{
            return INSTANCE
        }

        fun destroyDatabase(){
            INSTANCE = null
        }
    }
}