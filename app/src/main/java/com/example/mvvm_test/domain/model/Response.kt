package com.example.mvvm_test.domain.model

import com.example.mvvm_test.data.repository.model.Drink

data class Response(
    val drink: Drink? = null,
    val error: String? = null
)